import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)

const routes = [
  {
    path: '/tree1', 
    name: 'Tree1', 
    component: () => import('../views/Tree1.vue')
  },
  {
    path: '/tree2',
    name: 'Tree2',
    component: () => import('../views/Tree2.vue')
  },
  {
    path: '/tree3',
    name: 'Tree3',
    component: () => import('../views/Tree3.vue')
  },{
    path: '/tree4',
    name: 'Tree4',
    component: () => import('../views/Tree4.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
